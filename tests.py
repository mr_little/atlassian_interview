#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import unittest
from string_to_json import string_to_json


class TestString2Json(unittest.TestCase):

    def test_error_input(self):
        with self.assertRaises(TypeError):
            string_to_json({})

    def test_empty(self):
        json_str = string_to_json("")
        self.assertDictEqual({}, json.loads(json_str))

    def test_emoticon_not_exists(self):
        json_str = string_to_json("Ich habe (blabla) gesagt!")
        self.assertDictEqual({}, json.loads(json_str))

    def test_link_not_exists(self):
        json_str = string_to_json("What you think about this webpage? http://www.blablawebsite.ru")
        data_expected = {
            'links': [
                {
                    'url': 'http://www.blablawebsite.ru', 
                    'title': ''
                }
            ]
        }
        self.assertDictEqual(data_expected, json.loads(json_str))

    def test_mention_chris(self):
        json_str = string_to_json("@chris you around?")
        self.assertDictEqual({'mentions': ['chris']}, json.loads(json_str))

    def test_emoticons_coffee_megusta(self):
        json_str = string_to_json("Good morning! (megusta) (coffee)")
        self.assertDictEqual({'emoticons': ['megusta', 'coffee']}, json.loads(json_str))

    def test_olympics_link(self):
        json_str = string_to_json("Olympics are starting soon; http://www.nbcolympics.com")
        data_expected = {
            'links': [
                {
                    'url': 'http://www.nbcolympics.com', 'title': 'NBC Olympics | Home of the 2016 Olympic Games in Rio'
                }
            ]
        }
        self.assertDictEqual(data_expected, json.loads(json_str))

    def test_all_together(self):
        json_str = string_to_json("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016")
        data_expected = {
            'mentions': ['bob', 'john'],
            'emoticons': ['success'], 
            'links': [
                {
                    'url': 'https://twitter.com/jdorfman/status/430511497475670016', 
                    'title': 'Justin Dorfman on Twitter: "nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq"'
                }
            ]
        }
        self.assertDictEqual(data_expected, json.loads(json_str))

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestString2Json)
    unittest.TextTestRunner(verbosity=2).run(suite)