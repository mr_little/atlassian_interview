# -*- coding: utf-8 -*-

import re
import json
import urllib2
import lxml.html as html


class Link(object):

    def __init__(self, link):
        self.__title = ""
        self.__link = link

    @property
    def title(self):
        if not self.__title and self.__link:
            try:

                req = urllib2.Request(self.__link, None, {'accept-language':'en-US'})
                html_string = urllib2.urlopen(req).read()
                head = html.fromstring(html_string).head
                self.__title = head.find("title").text
            except IOError, e:
                # No reason try again
                self.__link = None
        
        return self.__title

    @property
    def url(self):
        return self.__link

    def __unicode__(self):
        return "%s(%s)" % (self.__link, self.__title)

    def __str__(self):
        return "%s(%s)" % (self.__link, self.__title.encode("utf-8"))


class Searcher(object):

    def search(self, text):
        return re.findall(self.re, text)


class Mentions(Searcher):

    def __init__(self):
        self.re = re.compile(r"@([a-z-A-Z0-9]+)")


class EmoticonsList(object):
    em_list = []

    @classmethod
    def fill_from_filesystem(cls):
        with open("emoticons.txt") as fd:
            map(lambda x: cls.em_list.append(x.strip()), fd.readlines())


class Emoticons(Searcher):

    def __init__(self):
        if len(EmoticonsList.em_list) == 0:
            EmoticonsList.fill_from_filesystem()

        self.re = re.compile(r"\((%s)\)" % "|".join(EmoticonsList.em_list))


class Links(Searcher):

    def __init__(self):
        # primitive link search
        self.re = re.compile(r"(http(?:s)?://[^\s]+)")

    def search(self, text):
        _list = map(lambda x: Link(x), super(self.__class__, self).search(text))
        return _list


def string_to_json(text, indent=1):
    result = {}

    m_list = Mentions().search(text)
    if m_list:
        result["mentions"] = m_list

    e_list = Emoticons().search(text)
    if e_list:
        result["emoticons"] = e_list

    l_list = Links().search(text)
    if l_list:
        result['links'] = map(lambda x: {"url": x.url, "title": x.title}, l_list)

    return json.dumps(result, sort_keys=True, indent=indent, ensure_ascii=False).encode('utf8')

